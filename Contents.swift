import UIKit

// MARK: Raw Strings

let rain = #"The "rain" in "Spain" falls mainly on the Spaniards."#

let keypaths = #"Swift keypaths such as \Person.name hold uninvoked references to properties."#

let answer = 42
let dontpanic = #"The answer to life, the universe, and everything is \#(answer)."#

let regex2 = #"\\[A-Z]+[A-Za-z]+\.[a-z]+"#

// MARK: Result Type

let results = Result { try String(contentsOfFile: "File") }

switch results {
case .failure(let error):
    print(error.localizedDescription)
case .success(let file):
    print("Found file: \(file)")
}

do {
    try String(contentsOfFile: "File")
} catch {
    print(error)
}

// MARK: String Interpolation

struct Pen {
    var inkColor: UIColor
    var inkLevel: Int
}

extension String.StringInterpolation {
    mutating func appendInterpolation(_ value: Pen) {
        appendInterpolation("Pen: \(value.inkColor) && \(value.inkLevel) / 100")
    }
}

let pen = Pen(inkColor: .blue, inkLevel: 87)
print(pen)
print("hello: \(pen)")

// MARK: Dynamic Callable

@dynamicCallable
struct Dog {
    func dynamicallyCall(withKeywordArguments args: KeyValuePairs<String, String>)  {
        print("StringDog \(args.first?.value ?? "")")
    }
    func dynamicallyCall(withKeywordArguments args: KeyValuePairs<String, Int>)  {
        print("IntDog \(args.first?.value ?? 6)")
    }
    func dynamicallyCall(withKeywordArguments args: KeyValuePairs<String, Any>)  {
        print("AnyDog \(args.first?.value)")
    }
    func dynamicallyCall(withArguments args: [Int]) {
        (0 ... (args.first ?? 1)).forEach { (i) in
            print("WOOF!!")
        }
    }
}

let dog = Dog()

dog(anything: "BARK")
dog(reallyAnything: 5)
dog(pi: 3.14)
dog(7)

// MARK: Future Enum Cases

enum PasswordError: Error {
    case short
    case obvious
    case simple
}

func showNew(error: PasswordError) {
    switch error {
    case .short:
        print("Your password was too short.")
    case .obvious:
        print("Your password was too obvious.")
    @unknown default:
        print("Your password wasn't suitable.")
    }
}

// MARK: Checking for integer multiples

let rowNumber = 4

if rowNumber.isMultiple(of: 2) {
    print("Even")
} else {
    print("Odd")
}

// MARK: Counting Matching items

let pythons = ["Eric Idle", "Graham Chapman", "John Cleese", "Michael Palin", "Terry Gilliam", "Terry Jones"]
let terryCount = pythons.count { $0.hasPrefix("Terry") }

// MARK: compactMapValues()

let times = [
    "Hudson": "38",
    "Clarke": "42",
    "Robinson": "35",
    "Hartis": "DNF"
]

let finishers1 = times.compactMapValues { Int($0) }
let finishers2 = times.compactMapValues(Int.init)

let people = [
    "Paul": 38,
    "Sophie": 8,
    "Charlotte": 5,
    "William": nil
]

let knownAges = people.compactMapValues { $0 }
